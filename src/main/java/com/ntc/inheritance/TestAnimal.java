/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.inheritance;

/**
 *
 * @author L4ZY
 */
public class TestAnimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Animal animal = new Animal("AA", "Red", 4);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("dang", "Grey");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("to", "Brown");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("mome", "White&Black");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("bat", "White&Black");
        bat.speak();
        bat.walk();

        Cat zero = new Cat("Zero", "Yellow");
        zero.speak();
        zero.walk();

        Duck som = new Duck("som", "orange");
        som.speak();
        som.walk();
        som.fly();
        
        Duck gabgab = new Duck("gabgab", "orange");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();

        System.out.println("som is animal: " + (som instanceof Animal));
        System.out.println("som is Duck: " + (som instanceof Duck));
        System.out.println("som is Object: " + (som instanceof Object));

        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("som is animal: " + (animal instanceof Animal));

        Animal[] animals = {dang,to,mome,bat, som,gabgab, zero};
        for(int i=0;i< animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }

}
