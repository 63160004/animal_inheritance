/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.inheritance;

/**
 *
 * @author L4ZY
 */
public class Duck extends Animal {
    private int numberOfWings = 2;
    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck created");
    }
    public  void fly(){
        
        System.out.println("Duck: "+ name + " fly!!!");
        
    }
    @Override
    public void walk() {
        super.walk();
        System.out.println("Dog: " + name + " walk with " + numberOfLegs + " Legs"
                +"and have "+numberOfWings +" wings");
    }
    @Override
    public void speak(){
        super.speak();
         System.out.println("Dog: " + name + " speak > kab kab!");
    }    

}
